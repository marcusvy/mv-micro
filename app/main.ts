/**
 * Main Class Application
 */
import { registerServiceWorker } from "./ts/service-worker";

export class App {
  constructor() {}

  register() {
    if (process.env.NODE_ENV != "development") {
      registerServiceWorker();
    }
  }

  start() {}
}
