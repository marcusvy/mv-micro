<?php

declare(strict_types=1);


$autoloadFile = sprintf("%s/../vendor/autoload.php", __DIR__);
require_once(realpath($autoloadFile));

use App\Core;

$core = new Core();
$core->start();
