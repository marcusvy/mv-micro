module.exports = {
  purge: [
    './app/**/*.html',
    './app/**/*.ts',
    './app/**/*.js',
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
