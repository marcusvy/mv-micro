<?php

namespace App;

class Core
{
  private string $html = '';

  public function __construct()
  {
    $this->html = getcwd() . '/bundle/index.html';
  }

  public function start()
  {
    if (!empty($this->html)) {
      echo file_get_contents($this->html);
    }
  }

  /**
   * Get the value of html
   */
  public function getHtml()
  {
    return $this->html;
  }

  /**
   * Set the value of html
   *
   * @return  self
   */
  public function setHtml($html)
  {
    $this->html = $html;

    return $this;
  }
}
