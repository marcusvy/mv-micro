import "./app/styles/style.scss";

import { App } from "./app/main";
const application = new App();
application.register();
application.start();
